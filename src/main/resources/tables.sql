CREATE TABLE products (
  description VARCHAR (256) NOT NULL,
  regularPrice VARCHAR (256),
  currentPrice VARCHAR (256),
  url VARCHAR (256) NOT NULL UNIQUE,
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);

CREATE TABLE users (
  name VARCHAR (256) NOT NULL,
  surname VARCHAR (256) NOT NULL,
  email VARCHAR (256) NOT NULL,
  password VARCHAR (256) NOT NULL,
  id INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id)
);

CREATE TABLE users_products (
  user_id INT NOT NULL,
  product_id INT NOT NULL,
  PRIMARY KEY (user_id, product_id),
  FOREIGN KEY (user_id) REFERENCES  users (id),
  FOREIGN KEY (product_id) REFERENCES  products (id)
);