package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class GetProductsMain extends Application {


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        URL resource = GetProductsMain.class.getResource("/login-form.fxml");
        Parent root = FXMLLoader.load(getClass().getResource("/login-form.fxml" ), ResourceBundle.getBundle("msg"));
        Scene scene = new Scene(root, 600, 400);
        stage.setScene(scene);
        stage.show();



    }
}
