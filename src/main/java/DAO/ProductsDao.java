package DAO;

import javafx.scene.control.TextInputDialog;
import model.*;
import service.AppEntityMangerFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.criteria.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductsDao {

    private Logger logger = Logger.getLogger(ProductsDao.class.getCanonicalName());
    private HttpClient client = new DefaultHttpClient();
    private ObjectMapper objectMapper = new ObjectMapper();

    private String scraperUrlBeg = "http://motyar.info/webscrapemaster/api/?url=";
    private String sraperXpathCurrentPrice = "&xpath=//*[@id=%22product-container%22]/section/div[3]/div/div[2]/div[1]/p";
    private String scraperXpathRegPrice ="&xpath=//*[@id=%22product-container%22]/section/div[3]/div/div[2]/div[1]/span";



    public String getSinglePrice(String url) {
        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            List<Price> priceList = objectMapper.readValue(response.getEntity().getContent(), new TypeReference<List<Price>>() {});
            if (CollectionUtils.isEmpty(priceList) || priceList.size() > 1) {
                return "";
            }
            return priceList.get(0).getText();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }


    /**
     * Method putting a dot in the int value of the price (1799 -> 17.99)
     * @param @price
     * @return - modified price string
     */
    public String modifyPriceString (String price) {
        char[] pArray = price.toCharArray();
        List<String> pString = new ArrayList<>(pArray.length+1);
        for (char c:pArray) {
            pString.add(String.valueOf(c));
        }
        if (pString.size() > 2){
            pString.add((pArray.length-2), ".");

        } else if (pString.size() == 2) {
            pString.add(0, "0");
            pString.add((pString.size()-2), ".");

        } else {
            pString.add(0, "0");
            pString.add(0, "0");
            pString.add((pString.size()-2), ".");
        }
        String priceWithDot = String.join("",pString);
        return priceWithDot;
    }

    /**
     * Database connectivity methods ofr products
     * @param @product
     */
    public Product addProduct(Product product) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(product);
        tx.commit();
        em.close();
        return product;
    }

    /**
     * Updating a product in a database
     * @param @product
     * @return @product
     */
    public Product updateProduct(Product product) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.merge(product);
        tx.commit();
        em.close();
        return product;
    }

    /**
     * This method looks for a product in the database by its unique url
     * @param @url
     * @return @Optional<Product>
     */
    public Optional<Product> findProductByUrl(String url) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Product> query = criteriaBuilder.createQuery(Product.class);
        Root<Product> root = query.from(Product.class);
        Predicate urlPredicate = criteriaBuilder.and(criteriaBuilder.equal(root.get(Product_.url), url));
        query.where(urlPredicate);
        Optional<Product> singleResult;
        try {
            singleResult = Optional.ofNullable(em.createQuery(query).getSingleResult());
        } catch (NoResultException ex) {
            logger.log(Level.INFO, ex.getMessage());
            singleResult = Optional.empty();
        }
        em.close();
        return singleResult;
    }

    /**
     * This method looks for a product in the database by its unique PK id
     * @param @id
     * @return @Product
     */
    public Product findProductById(int id) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        Product product = em.find(Product.class, id);
        return product;

    }

    /**
     * This method returns all products available in the database, to be use in the future
     * @return @List<Product>
     */
    public List<Product> findAllAvailableProducts() {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery(Product.class);
        query.from(Product.class);
        List<Product> productList = em.createQuery(query).getResultList();
        em.close();
        return productList;
    }

    /**
     * This method finds all products for a chosen user by their PK id
     * @param @id
     * @return @List<Product>
     */
    public List<Product> findAllProductsByUser (int id) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> query = cb.createQuery(Product.class);
        Root<User> user = query.from(User.class);
        ListJoin<User, Product> join = user.join(User_.products);
        query.where(user.get(User_.id).in(id));
        query.select(join);
        List<Product> productList =  em.createQuery(query).getResultList();
        em.close();
        return productList;
    }

    /**
     * Method removing a product wit a chosen PK id from the database.
     * @param @id
     */
    public void removeProduct(int id) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        Product product = em.find(Product.class, id);
        em.remove(updateProduct(product));
        tx.commit();
        em.close();
    }

    /**
     * This method takes in the product data from the user in the form of two text input pop-ups,
     * first asking for the product url, and then for the description of the product
     * @return @Product
     */
    public Product productAddedByUser(){
        Product product = new Product();

        Optional<String> url = addUrlPopUp();
        if (url.isPresent()) {
            setProductData(product, url.get());
        } else {
            return null;
        }

        Optional<String> description = addProductDescriptionPopup();
        if (description.isPresent()){
            product.setDescription(description.get());
        } else {
            return null;
        }
        return product;
    }

    /**
     * Sets the product current and regular price and its url
     * @param @product
     * @param @url
     */
    public void setProductData(Product product, String url) {
        product.setCurrentPrice(modifyPriceString(getSinglePrice(scraperUrlBeg+url+sraperXpathCurrentPrice)));
        product.setRegularPrice(modifyPriceString(getSinglePrice(scraperUrlBeg+url+scraperXpathRegPrice)));
        product.setUrl(url);
    }

    /**
     * Method dispalying a pop-up for user to add a description to a product
     * @return
     */
    public Optional<String> addProductDescriptionPopup(){
        TextInputDialog inputName = new TextInputDialog();
        inputName.setTitle("Nazwa produktu");
        inputName.setHeaderText(null);
        inputName.setContentText("Podaj nazwe produktu");
        return inputName.showAndWait();
    }

    /**
     * Method displaying a pop-up for user to add an url for the desired product
     * @return
     */
    public Optional<String> addUrlPopUp(){
        TextInputDialog input = new TextInputDialog();
        input.setTitle("Adres URL produktu");
        input.setHeaderText(null);
        input.setContentText("Podaj URL produktu");
        return input.showAndWait();


    }

}
