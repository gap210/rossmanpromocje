package DAO;

import service.AppEntityMangerFactory;
import model.User;
import model.User_;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class UsersDao {

    private static final Logger LOG = Logger.getLogger(UsersDao.class.getName());


    /**
     * Add user to databse
     * @param @user
     */
    public void addUser(User user) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.persist(user);
        tx.commit();
        em.close();
    }

    /**
     * Update user in the databse
     * @param @user
     * @return @User
     */
    public User updateUser(User user) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        em.merge(user);
        tx.commit();
        em.close();
        return user;
    }

    /**
     * Method to find all users in the databse, to be used in the future for admin purposes
     * @return
     */
    public List<User> findAllUsers() {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery query = cb.createQuery(User.class);
        query.from(User.class);
        List<User> userList = em.createQuery(query).getResultList();
        em.close();
        return userList;
    }

    /**
     * Removes user with a selected if from the database
     * @param id
     */
    public void removeUser(int id) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        User user = em.find(User.class, id);
        em.remove(updateUser(user));
        tx.commit();
        em.close();
    }

    /**
     * Method returns a User with a specified email and password if they exist
     * @param @email
     * @param @password
     * @return Optional <User>
     */
    public Optional<User> findUserByEmailAndPassword(String email, String password) {
        EntityManager em = AppEntityMangerFactory.createEntityManager();
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);
        Predicate emailPredicate = criteriaBuilder.and(criteriaBuilder.equal(root.get(User_.email), email));
        Predicate passwordPredicate = criteriaBuilder.and(criteriaBuilder.equal(root.get(User_.password), password));
        query.where(emailPredicate, passwordPredicate);
        Optional<User> singleResult;
        try {
            singleResult = Optional.ofNullable(em.createQuery(query).getSingleResult());
        } catch (NoResultException ex) {
            LOG.warning(ex.getMessage());
            singleResult = Optional.empty();
        }
        em.close();

        return singleResult;
    }
}
