package service;

import model.Price;
import model.PromoDates;
import org.apache.commons.collections.CollectionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PromotionDatesChecker {

    private Logger logger = Logger.getLogger(PromotionDatesChecker.class.getCanonicalName());
//    private HttpClient client = new DefaultHttpClient();
//    private ObjectMapper objectMapper = new ObjectMapper();



    public String getPromotinDatesString() {
        HttpClient client = new DefaultHttpClient();
        ObjectMapper objectMapper = new ObjectMapper();
        String url = "http://motyar.info/webscrapemaster/api/?url=https://www.rossmann.pl/promocje&xpath=//*[@id=%22magazineContainer%22]/div[2]/div/div[1]/h2";

        HttpGet httpGet = new HttpGet(url);
        try {
            HttpResponse response = client.execute(httpGet);
            List<PromoDates> promotionDates = objectMapper.readValue(response.getEntity().getContent(), new TypeReference<List<PromoDates>>() {});
            if (CollectionUtils.isEmpty(promotionDates) || promotionDates.size() > 1) {
                return "";
            }
            return promotionDates.get(0).getText();
        } catch (IOException e) {
            logger.log(Level.INFO, e.getMessage());
            return "";
        }
    }




}
