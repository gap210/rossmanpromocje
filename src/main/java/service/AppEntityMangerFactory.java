package service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AppEntityMangerFactory {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("RossmanPU");

    public static EntityManager createEntityManager () {
        return emf.createEntityManager();
    }
    public static void close() {
        emf.close();
    }
}
