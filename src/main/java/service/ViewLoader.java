package service;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ResourceBundle;
import main.*;


public class ViewLoader {

    public static void loadLoginView(Node node) throws IOException {
        loadView("/login-form.fxml", node);
    }

    public static void  loadCreateUserView(Node node) throws IOException {
        loadView("/create-user-form.fxml", node);
    }

    public static void loadMainLayoutView(Node node) throws IOException {
        loadView("/app.fxml", node);
    }

    private static void loadView(String viewPath, Node node) throws IOException {
        ResourceBundle rb = ResourceBundle.getBundle("msg");

        FXMLLoader loader = new FXMLLoader(GetProductsMain.class.getResource(viewPath), rb);
        Parent createUserView = loader.load();
        Scene scene = node.getScene();
        scene.setRoot(createUserView);
        Stage stage = (Stage) scene.getWindow();
        stage.sizeToScene();
    }
}