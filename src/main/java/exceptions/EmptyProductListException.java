package exceptions;

public class EmptyProductListException extends Exception {

    public EmptyProductListException() {
    }

    public EmptyProductListException(String s) {
        super(s);
    }
}
