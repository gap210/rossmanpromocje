package model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import java.util.Objects;

@JsonIgnoreProperties (ignoreUnknown = true)
public class Price  {

    private String text;

    private String html;

    public Price(String text, String html) {
        this.text = text;
        this.html = html;
    }

    public Price() {
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equals(text, price.text) &&
                Objects.equals(html, price.html);
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, html);
    }

    @Override
    public String toString() {
        return "model.Price{" +
                "text='" + text + '\'' +
                ", html='" + html + '\'' +
                '}';
    }
}
