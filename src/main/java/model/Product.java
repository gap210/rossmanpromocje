package model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "products", uniqueConstraints = @UniqueConstraint(name = "url", columnNames = ("url")))
public class Product{

    @Column
    private String description;

    @Column
    private String regularPrice;

    @Column
    private String currentPrice;

    @Column(name = "url")
    private String url;

    @ManyToMany(mappedBy = "products", fetch = FetchType.EAGER)
    private List<User> users = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public Product() {
    }

    public Product(String description, String currentPrice, String regularPrice, String url) {
        this.description = description;
        this.currentPrice = currentPrice;
        this.url = url;
        this.regularPrice = regularPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getRegularPrice() {
        return regularPrice;
    }

    public void setRegularPrice(String regularPrice) {
        this.regularPrice = regularPrice;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(description, product.description) &&
                Objects.equals(regularPrice, product.regularPrice) &&
                Objects.equals(currentPrice, product.currentPrice) &&
                Objects.equals(url, product.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, regularPrice, currentPrice, url);
    }

    @Override
    public String toString() {
        return "Product{" +
                "description='" + description + '\'' +
                ", regularPrice='" + regularPrice + '\'' +
                ", currentPrice='" + currentPrice + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
