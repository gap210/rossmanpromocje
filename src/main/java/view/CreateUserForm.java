package view;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import DAO.UsersDao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import service.Errors;
import service.*;
import model.*;


public class CreateUserForm implements Initializable {

    @FXML
    private TextField name;
    @FXML
    private TextField surname;
    @FXML
    private TextField email;
    @FXML
    private PasswordField password;
    @FXML
    private PasswordField rePassword;

    private UsersDao userDao;

    public void initialize(final URL location, final ResourceBundle resources) {
        userDao = new UsersDao();
    }

    public void createUser(final ActionEvent event) throws IOException {
        Errors errors = new Errors();
        validate(errors);
        if (!errors.hasErrors()) {
            createUser();
            ViewLoader.loadLoginView((Node) event.getTarget());
        } else {
            showError("Alert", errors.getErrors().toString());
        }
    }

    private void createUser() {
        User user = new User(name.getText(),surname.getText(), email.getText(), password.getText());
        Errors errors = new Errors();

        validate(errors);

        if (errors.hasErrors()) {
            errors.getErrors();
        } else {
            Optional<User> existingUser = userDao.findUserByEmailAndPassword(email.getText(), password.getText());

            if (existingUser.isPresent()) {
                userAlreadyExists();
            } else {
                userDao.addUser(user);
            }
        }

    }

    private void userAlreadyExists() {
        showError("User error", "User with given email already exists!");
    }

    private void showError(final String title, final String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, content);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.show();
    }

    private boolean validatePassword(Errors errors) {
        return password.getText().equals(rePassword.getText());
    }


    private void validate(Errors errors) {
        validateEmail(errors);
        validatePassword(errors);
        isNameNotEmpty(errors);
        isSurameNotEmpty(errors);
        isPasswordNotEmpty(errors);
    }

    private boolean validateEmail(Errors errors) {

        FieldsVerification ver = new FieldsVerification();
        if (ver.isValidEmailAddress(email.getText())) {
            return true;
        } else {
            errors.addError("Email nie teges");
            return false;
        }
    }

    public boolean isNameNotEmpty(Errors errors) {
        FieldsVerification ver = new FieldsVerification();
        if (ver.isNameNotEmpty(name)) {
            return true;
        } else {
            errors.addError("Imie nie teges");
            return false;
        }
    }
    public boolean isSurameNotEmpty(Errors errors) {
        FieldsVerification ver = new FieldsVerification();
        if (ver.isSurnameNotEmpty(name)) {
            return true;
        } else {
            errors.addError("Surname nie teges");
            return false;
        }
    }

    public boolean isPasswordNotEmpty(Errors errors) {
        FieldsVerification ver = new FieldsVerification();
        if (ver.isPasswordNotEmpty(password)) {
            return true;
        } else {
            errors.addError("Hasło nie teges");
            return false;
        }
    }


}
