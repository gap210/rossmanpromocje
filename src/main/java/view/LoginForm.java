package view;

import DAO.UsersDao;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import main.GetProductsMain;
import model.User;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import service.*;

public class LoginForm implements Initializable {

    @FXML
    private TextField email;
    @FXML
    private PasswordField password;

    private UsersDao userDao;

    private LayoutView layoutView;

    private User activeUser;

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        this.userDao = new UsersDao();
    }

    public void onLogin(final ActionEvent event) throws IOException {
        Optional<User> user = userDao.findUserByEmailAndPassword(email.getText(), password.getText());

        if (user.isPresent()) {
            System.out.println("LOGGED IN");
            activeUser = user.get();
            ResourceBundle rb = ResourceBundle.getBundle("msg");
            FXMLLoader loader = new FXMLLoader(GetProductsMain.class.getResource("/app.fxml"), rb);
            Parent createUserView = loader.load();
            Scene scene = ((Node) event.getTarget()).getScene();
            LayoutView controller = loader.getController();
            controller.setLoggedInUser(activeUser);
            scene.setRoot(createUserView);
            Stage stage = (Stage) scene.getWindow();
            stage.sizeToScene();

        } else {
            System.out.println("LOGIN FAILED");
        }
    }


    public void onCreateAccount(final ActionEvent event) throws IOException {
        ViewLoader.loadCreateUserView((Node) event.getTarget());
    }

    public LayoutView getLayoutView() {
        return layoutView;
    }

    public void setLayoutView(LayoutView layoutView) {
        this.layoutView = layoutView;
    }
}
