package view;

import DAO.ProductsDao;
import DAO.UsersDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Product;
import model.User;
import service.PromotionDatesChecker;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class LayoutView implements Initializable {

    @FXML
    private TableView<Product> productTable;

    @FXML
    private TableColumn<Product, String> description;

    @FXML
    private TableColumn<Product, String> currentPrice;

    @FXML
    private TableColumn<Product, String> regularPrice;

    @FXML
    private Button removeDataButton;

    @FXML
    private Button getDataButton;

    @FXML
    private Button addDataButton;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private TextField userName;

    @FXML
    private TextField promoDates;

    @FXML
    private User activeUser;

    private List<Product> productList;
    private ProductsDao dao = new ProductsDao();
    private UsersDao usersDao = new UsersDao();
    private String email;
    private int id;
    private PromotionDatesChecker promotionDatesChecker = new PromotionDatesChecker();



    /**
     * This method accepts a string email to be passed to the main view
     * @param user
     */
    public void setLoggedInUser(User user) {
        activeUser = user;
        userName.setText("Witaj " + activeUser.getName() + " :)");


    }

    public void initialize(URL url, ResourceBundle resourceBundle) {
        currentPrice.setCellValueFactory(new PropertyValueFactory<>("currentPrice"));
        regularPrice.setCellValueFactory(new PropertyValueFactory<>("regularPrice"));
        description.setCellValueFactory(new PropertyValueFactory<>("description"));
        promoDates.setText(promotionDatesChecker.getPromotinDatesString());


    }

    /**
     * Gets all products saved in the database for the currently logged in user
     */
    public void getAllLoggedInUserProducts() {
        if (productList == null) {
            productList = dao.findAllProductsByUser(activeUser.getId());

        } else {
            productList = new ArrayList<>();
            productList.addAll(dao.findAllProductsByUser(activeUser.getId()));
        }
        populateTableView(productList);

    }

    /**
     * adds a single new or already existing product to the displayed tableView
     */
    public void addSingleDataToTableView() {
        Product product = dao.productAddedByUser();
        if (product == null) {
            return;
        }
        Optional<Product> dbProductCheck = checkIfProductAlreadyInDb(product);
        if (dbProductCheck.isPresent()) {
            activeUser
                    .getProducts()
                    .add(dbProductCheck.get());
            usersDao
                    .updateUser(activeUser);
            updatingProductListForLoggedInUser();
            populateTableView(productList);
        } else {
            dao.addProduct(product);
            activeUser
                    .getProducts()
                    .add(product);
            usersDao
                    .updateUser(activeUser);
            updatingProductListForLoggedInUser();
            populateTableView(productList);
        }
    }

    public Optional<Product> checkIfProductAlreadyInDb (Product product) {
        Optional<Product> dbProductCheck = dao.findProductByUrl(product.getUrl());
        if (dbProductCheck.isPresent()) {
            return dbProductCheck;
        } else {
            return Optional.empty();
        }

    }


    public void updatingProductListForLoggedInUser() {
        if (productList == null) {
            productList = dao.findAllProductsByUser(activeUser.getId());
        } else {
            productList = new ArrayList<>();
            productList.addAll(dao.findAllProductsByUser(activeUser.getId()));
        }
    }

    public void populateTableView (List<Product> products) {
        ObservableList<Product> observableProducts = FXCollections.observableArrayList();
        observableProducts.addAll(products);
        productTable.setItems(observableProducts);
    }

    public void removeSelectedProductFromUsersList() {
        Product product = productTable.getSelectionModel().getSelectedItem();
        productList.remove(product);
        activeUser.setProducts(productList);
        populateTableView(productList);
        usersDao.updateUser(activeUser);


    }

    public User getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
