package view;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import service.ViewLoader;
import static org.powermock.api.mockito.PowerMockito.*;
import java.io.IOException;
import static org.junit.Assert.*;


@RunWith(PowerMockRunner.class)
@PrepareForTest(ViewLoader.class)
public class LoginFormTest {

    @Mock
    private ViewLoader viewLoader;

    @Mock
    private ActionEvent actionEvent;

    @Mock
    private Node node = (Node) actionEvent.getTarget();




    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void onLogin() {

    }

    @Test
    public void onCreateAccount() throws IOException{
    }

    @Test
    public void getLayoutView() {
    }

    @Test
    public void setLayoutView() {
    }
}