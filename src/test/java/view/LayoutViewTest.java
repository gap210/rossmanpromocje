package view;

import DAO.ProductsDao;
import de.saxsys.javafx.test.JfxRunner;
import model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

import static org.junit.Assert.*;

@RunWith(JfxRunner.class)
public class LayoutViewTest {

    @Mock
    private ProductsDao productsDao;

    @InjectMocks
    private LayoutView layoutView;

    private User user;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        user = new User("a","b","c","d");

    }

    @After
    public void tearDown() throws Exception {

        layoutView = null;
        user = null;
    }

    @Test
    public void populateTableViewShouldProperlyCallDaoMethod() {
        user.setId(1);
        layoutView.setActiveUser(user);
        layoutView.getAllLoggedInUserProducts();
        assertNotNull(layoutView.getActiveUser());
        verify(productsDao).findAllProductsByUser(layoutView.getActiveUser().getId());
    }

    @Test
    public void addDataToList() {
    }

    @Test
    public void getId() {
    }

    @Test
    public void setId() {
    }

    @Test
    public void getEmail() {
    }

    @Test
    public void setEmail() {
    }
}