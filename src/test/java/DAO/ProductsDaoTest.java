package DAO;

import model.Product;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.mockito.MockitoAnnotations;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;


public class ProductsDaoTest {


    private ProductsDao productsDao = new ProductsDao();


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getSinglePrice() {

    }

    @Test
    public void modifyPriceStringShouldReturnAppropriateString() {
        String price = "1899";
        String modifiedPrice = "18.99";
        assertEquals(modifiedPrice, productsDao.modifyPriceString(price));
    }
    @Test
    public void modifyPriceStringShouldProperlyAddStringZeroToTheBeginningOfAStringWithTwoChars() {
        String price = "99";
        String modifiedPrice = "0.99";
        assertEquals(modifiedPrice, productsDao.modifyPriceString(price));
    }
    @Test
    public void modifyPriceStringShouldProperlyAddTwoStringZerosToTheBeginningOfAStringWithOneChar() {
        String price = "9";
        String modifiedPrice = "0.09";
        assertEquals(modifiedPrice, productsDao.modifyPriceString(price));

    }
    @Test
    public void shouldProperlyAddUpdateAndRemoveProduct_addProductTest_updateProductTest_removeProductTest() {
        Product product = new Product("a","b","d","c");
        productsDao.addProduct(product);
        Product foundProduct = productsDao.findProductByUrl("c").get();
        assertEquals(product, foundProduct);

        foundProduct.setDescription("d");
        productsDao.updateProduct(foundProduct);
        Product updatedProduct = productsDao.findProductByUrl("c").get();
        assertEquals(foundProduct, updatedProduct);

        Product toBeRemovedProduct = productsDao.findProductByUrl("c").get();
        int id = toBeRemovedProduct.getId();
        productsDao.removeProduct(id);
        assertNull(productsDao.findProductById(id));
    }


    @Test
    public void findAllAvailableProducts() {
    }

    @Test
    public void findAllProductsByUser() {
    }

    @Test
    public void addSingleDataToList() {
    }

    @Test
    public void findProductByUrl() {
    }

    @Test
    public void findProductById() {

    }
}